<?php

// To share data to all or specific view 
// create a middleware
// register it in Kernel .php
// fill handle method like this:
public function handle($request, Closure $next)
    {
        View::composer('*', function($view) {
            $query = Query to Database;
            $view->with('variable_name_called_in_views', $query);
        });

        return $next($request);
    }

    // Insted this sign '*' you can print rout to view 
    // like 'folder.folder.name_of_file'
